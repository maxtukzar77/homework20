// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	NewMoveDirection = EMovementDirection::DOWN;
	CurrentMoveDirection = NewMoveDirection;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnaleElementClass, NewTransform);
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		if(ElementIndex == 0)
			NewSnakeElement->SetFirstElementType();
	}
}

void ASnakeBase::Move()
{
	FVector MovmentVector(ForceInitToZero);
	float MovementSpeedDelta = ElementSize;

	switch (NewMoveDirection)
	{
	case EMovementDirection::UP:
		MovmentVector.X += MovementSpeedDelta;
		break;
	case EMovementDirection::DOWN:
		MovmentVector.X -= MovementSpeedDelta;
		break;
	case EMovementDirection::LEFT:
		MovmentVector.Y += MovementSpeedDelta;
		break;
	case EMovementDirection::RIGHT:
		MovmentVector.Y -= MovementSpeedDelta;
		break;
	}

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovmentVector);
	CurrentMoveDirection = NewMoveDirection;
}

